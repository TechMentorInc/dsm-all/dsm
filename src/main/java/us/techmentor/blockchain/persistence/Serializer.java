package us.techmentor.blockchain.persistence;

public interface Serializer<T>{
    public byte[] asBytes(T t);
}