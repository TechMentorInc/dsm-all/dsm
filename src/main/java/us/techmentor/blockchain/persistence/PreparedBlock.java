package us.techmentor.blockchain.persistence;

import us.techmentor.blockchain.Block;

public class PreparedBlock {
    Block block;
    public Block getBlock() {
        return block;
    }
    public void setBlock(Block block) {
        this.block = block;
    }
    public byte[] getData() {
        return data;
    }
    public void setData(byte[] data) {
        this.data = data;
    }
    byte[] data;
}
