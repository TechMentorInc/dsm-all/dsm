package us.techmentor.blockchain;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.techmentor.Hash;
import us.techmentor.blockchain.connector.Protocol;
import us.techmentor.blockchain.speed.Statistics;
import us.techmentor.blockchain.speed.complexity.Action;
import us.techmentor.blockchain.speed.complexity.ComplexityDefinition;
import us.techmentor.blockchain.speed.complexity.ComplexityProposal;

import java.util.ArrayList;
import java.util.List;


public class RemoteProposition {
    @Inject BlockPreparation blockPreparation;
    @Inject Statistics statistics;
    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    public void incomingPoll(ComplexityProposal complexityProposal, Protocol protocol) {
        switch(complexityProposal.action()){
            case PROPOSE -> {
                logger.info("Proposing complexity modification: {}",complexityProposal.id().getValue());
                propose(complexityProposal, protocol);
            }
            case ACCEPT -> {
                logger.info("Received remote proposition acceptance: {}",complexityProposal.id().getValue());
                acceptedIds.add(complexityProposal.id());
            }
        }
    }

    private void propose(ComplexityProposal complexityProposal, Protocol protocol) {
        // make sure that we're good to go?  check statistics?
        // then put an acceptance on the outgoing complexity queue
        logger.info("Proposing.."+complexityProposal);
        if(checkStatistics(complexityProposal.complexityDefinition())) {
            logger.info("Locally Accepting Proposal");
            protocol.sendComplexity(complexityProposal.changeAction(Action.ACCEPT));
            blockPreparation.setComplexity(complexityProposal.complexityDefinition());
        }
    }
    private boolean checkStatistics(ComplexityDefinition complexityDefinition) {
        var trend = statistics.meanRelativeToTarget();
        var currentComplexity = blockPreparation.getComplexity().nonceTarget();
        var targetComplexity = complexityDefinition.nonceTarget();

        var slowerThanTarget = (trend>0);
        var fasterThanTarget = (trend<0);

        var speedUp = (targetComplexity.compareTo(currentComplexity)==1);
        var slowDown = (targetComplexity.compareTo(currentComplexity)==-1);

        logger.info("SlowerThanTarget:"+slowerThanTarget+" | FasterThanTarget:"+fasterThanTarget+" | SpeedUp:"+speedUp+" | SlowDown:"+slowDown);
        return (( slowerThanTarget && speedUp) || (fasterThanTarget && slowDown ));
    }
    List<Hash> acceptedIds = new ArrayList<>();
    public boolean isAccepted(Hash id){
        return acceptedIds.contains(id);
    }
}
