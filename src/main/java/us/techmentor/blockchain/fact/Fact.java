package us.techmentor.blockchain.fact;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import us.techmentor.Hash;

public class Fact {
    Hash hash;
    Map<String,String> value = new HashMap<>();

	public Fact(Map<String,String> substance) {
        value = new HashMap<>(substance);
        if(value.get("hash")==null){
            hash = Hash.create(value.toString());
            value.put("hash",hash.getValue());
        }else{
            this.hash = Hash.fromString(value.get("hash"));
        }
        this.value = Collections.unmodifiableMap(this.value);
    }
    public Fact(){
        hash = Hash.create(value.toString());
    }

    public static Fact of(String... input) {
        if(input.length%2!=0)
            throw new RuntimeException("List of fact details is odd.");
        var map = new HashMap<String,String>();
        for(int i=0; i<input.length; i+=2)
            map.put(input[i],input[i+1]);
        return new Fact(map);
    }

    public String get(String key){
        return value.get(key);
    }
    
	public Hash getHash() {
		return this.hash;
    }
    public void setHash(Hash hash) {
        this.hash = hash;
    }

    public String toString() {
        return hash.getValue();
    }
    public Map<String,String> getValueMap() {return value;}

    @Override
    public boolean equals(Object o){
        var comp = (Fact)o;
        return value.equals(comp.value) && hash.equals(comp.hash);
    }

}
