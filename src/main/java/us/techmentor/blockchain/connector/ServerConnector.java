package us.techmentor.blockchain.connector;

public class ServerConnector extends BlockchainConnector<ServerProtocol>{
    public void init() {
        this.pollingActive=true;
    }

    public void setLastCommunicationTimeIsNow() {
        setLastCommunicationTime(System.currentTimeMillis());
    }
    protected long lastCommunicationTime=0;
    public long getLastCommunicationTime() {
        return this.lastCommunicationTime;
    }
    public void setLastCommunicationTime(long lastCommunicationTime){
        this.lastCommunicationTime=lastCommunicationTime;
    }
}