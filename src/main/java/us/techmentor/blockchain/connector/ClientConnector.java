package us.techmentor.blockchain.connector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientConnector  extends BlockchainConnector{
    Logger logger = LoggerFactory.getLogger(this.getClass());
    public static class NoRegistration extends ClientConnector{
        @Override
        public void init(){}
    }
    boolean registered = false;
    public void init() {
        this.registered = protocol.register();
        logger.info("Registered[{}]", this.registered);
        this.pollingActive = this.registered;
    }

}