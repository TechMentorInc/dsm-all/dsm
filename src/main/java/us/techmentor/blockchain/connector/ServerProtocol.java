package us.techmentor.blockchain.connector;

import us.techmentor.blockchain.Chain;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.speed.complexity.ComplexityDefinition;
import us.techmentor.blockchain.speed.complexity.ComplexityProposal;

import java.util.Optional;

public interface ServerProtocol extends Protocol {
    void addFactToIncomingQueue(Fact fact);
    void addChainToIncomingQueue(Chain chain);
    void addComplexitySuggestionToIncomingQueue(ComplexityProposal complexityProposal);

    Optional<Fact> getNextFactFromOutgoingQueue();
    Optional<Chain> getNextChainFromOutgoingQueue();
    Optional<ComplexityProposal> getNextComplexityProposalFromOutgoingQueue();
}
