package us.techmentor.blockchain.connector;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.techmentor.blockchain.*;
import us.techmentor.blockchain.speed.complexity.Action;
import us.techmentor.blockchain.speed.complexity.ComplexityProposal;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.speed.SpeedAdaptingConnector;

public abstract class BlockchainConnector<P extends Protocol> implements SpeedAdaptingConnector {
    @Inject ChainOperations chainOperations;
    @Inject FactOperations factOperations;
    @Inject RemoteProposition remoteProposition;

    ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    boolean pollingActive=false;

    P protocol;
    public P getProtocol() { return this.protocol; }
    public abstract void init();
    public void stopPolling(){
        this.pollingActive=false;
        this.executorService.shutdownNow();
    }
    public void sendFact(Fact fact) { this.protocol.sendFact(fact); }
    public void updateChain(Chain chain) { this.protocol.updateChain(chain); }
    public void pullChain(){protocol.pollIncomingChain().ifPresent(chainOperations::updateChain);}

    private void poll(){
        if(this.pollingActive) {
            try {
                protocol.pollIncomingChain().ifPresent(chainOperations::updateChain);
                protocol.pollIncomingFact().ifPresent(factOperations::saveFact);
                protocol.pollIncomingComplexity().ifPresent(
                        cp -> remoteProposition.incomingPoll(cp,protocol)
                );
            } catch (Throwable t) {
                logger.info("Connector fail: "+this.getClass());
                t.printStackTrace();
            }
        }
    }
    public void go(P protocol){
        this.protocol = protocol;
        this.executorService
            .scheduleWithFixedDelay(
            this::poll,100,100,
            TimeUnit.MILLISECONDS);
        init();
    }

    @Override
    public void proposeComplexity(
        ComplexityProposal complexityProposal,
        Runnable success, Runnable failure) {

        var actionComplexityProposal = complexityProposal.changeAction(Action.PROPOSE);
        protocol.sendComplexity(actionComplexityProposal);

        var t= new Thread(() -> {
            int i = 0;
            while (!remoteProposition.isAccepted(complexityProposal.id())) {
                i++;
                try {
                    Thread.sleep(1000);
                    if (i > 100) {
                        failure.run();
                        break;
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            success.run();
        });
        t.setDaemon(true);
        t.start();
    }
}
