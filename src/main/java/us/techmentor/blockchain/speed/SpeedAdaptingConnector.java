package us.techmentor.blockchain.speed;

import us.techmentor.blockchain.speed.complexity.ComplexityProposal;

public interface SpeedAdaptingConnector {

    void proposeComplexity(
        ComplexityProposal complexityProposal,
        Runnable success, Runnable failure);
}
