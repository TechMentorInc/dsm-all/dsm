package us.techmentor.blockchain.speed.complexity;

import org.joda.time.DateTime;
import us.techmentor.Hash;

import java.math.BigInteger;

public record ComplexityProposal(Hash id, Action action, DateTime time, ComplexityDefinition complexityDefinition) {
    @Override
    public String toString(){
        return id.getValue()+","+action.label+","+time.getMillis()+","+complexityDefinition.nonceTarget().toString(16);
    }
    public static ComplexityProposal fromString(String in){
        var elements = in.split(",");
        return new ComplexityProposal(
                Hash.fromString(elements[0]),
                Action.valueOfLabel(elements[1]),
                new DateTime(Long.parseLong(elements[2])),
                new ComplexityDefinition(new BigInteger(elements[3],16))
        );
    }

    public ComplexityProposal changeAction(Action changeTo) {
        return
        new ComplexityProposal(
            this.id(),
            changeTo,
            this.time(),
            this.complexityDefinition());
    }
}
