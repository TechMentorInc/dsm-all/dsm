package us.techmentor;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.Iterator;

public class LogFiltering {
    public static void filterByStringCategory(String category){
        LoggerFactory.getLogger(LogFiltering.class).info("Adding category filter: {}", category);
        configureLogging(category);
    }
    private static void configureLogging(String category){
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        Logger rootLogger = context.getLogger(Logger.ROOT_LOGGER_NAME);

        MDCFilter mdcFilter = new MDCFilter(category);
        mdcFilter.start();

        for (Iterator<Appender<ILoggingEvent>> it = rootLogger.iteratorForAppenders(); it.hasNext(); ) {
            Appender<ILoggingEvent> appender = it.next();
            appender.addFilter(mdcFilter);
        }
    }
    private static class MDCFilter extends Filter<ILoggingEvent> {
        private final String requiredCategory;

        public MDCFilter(String requiredCategory) {
            this.requiredCategory = requiredCategory;
        }

        @Override
        public FilterReply decide(ILoggingEvent event) {
            String mdcCategory = MDC.get("category");
            if (requiredCategory.equals(mdcCategory)) {
                return FilterReply.ACCEPT;
            }
            return FilterReply.DENY;
        }
    }
}
