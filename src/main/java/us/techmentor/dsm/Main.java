package us.techmentor.dsm;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import com.google.inject.Guice;
import picocli.CommandLine;
import picocli.CommandLine.*;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Model.OptionSpec;
import picocli.CommandLine.Model.ArgSpec;
import us.techmentor.dsm.person.Actor;

@Command(name = "dsm",
        version = "dsm 1.0",
        description = "Distributed Social Media, by Kyle Rowland - TechMentor, Inc.",
        subcommands = {HelpCommand.class,InitializeCommand.class, RemoteCommand.class, TimelineCommand.class, ServeCommand.class, ActorCommand.class})
public class Main{
    @Spec CommandSpec spec;
    @CommandLine.Option(names="--timeout", required=false, description="Connection Timeout", paramLabel="<timeout-in-seconds>") int timeout;
    @CommandLine.Option(names="--synch-period", defaultValue = "5000", required=false, description="Synchronization Period", paramLabel="<synch-period-in-seconds>") int synchPeriod;
    @CommandLine.Option(names="--log-filter", required=false, description="Filter Logs By Category", paramLabel="<category-name>") String filterCategory;
    public static void main(String[] args) {
        int exitCode = new CommandLine(new Main()).execute(args);
        System.exit(exitCode);
    }

    public static DSM initializationRoutine(Main parent){
        var injector = Guice.createInjector();
        var dsm = injector.getInstance(DSM.class);
        dsm.init(parent.timeout, parent.synchPeriod);
        if(parent.filterCategory!=null && !parent.filterCategory.trim().equals(""))
            dsm.logFilter(parent.filterCategory);
        return dsm;
    }
}
@Command(name = "init", 
        description = "Initialize a distributed social network",
        subcommands = {HelpCommand.class})
class InitializeCommand implements Callable<Integer>{
    @CommandLine.ParentCommand
    private Main parent;

    @Override
    public Integer call() throws Exception {
        var dsm = Main.initializationRoutine(parent);
        System.out.println("Initialize DSM!");
        return (Integer)0;
    }
}

@Command(name = "serve",
        description = "Serve the distributed social network",
        subcommands = {HelpCommand.class})
class ServeCommand implements Callable<Integer>{
    @CommandLine.ParentCommand
    private Main parent;

    @Override
    public Integer call() throws Exception {
        var dsm = Main.initializationRoutine(parent);
        dsm.listen();
        System.out.println("Server stopped.");
        return (Integer)0;
    }
}
@Command(name = "actor",
        description = "Manage actors",
        subcommands = {HelpCommand.class})
class ActorCommand {
    @CommandLine.ParentCommand
    private Main parent;

    @Command(name="list", description="Show all actors", defaultValueProvider = DefaultValueProvider.class)
    public int list(@CommandLine.Option(names="-u", required=true, description="User Name", paramLabel="<user>") String user,
                    @CommandLine.Option(
                            names="-p", required=true, description="Password",
                            paramLabel="<password>", interactive=true) String password){
        System.out.println("All Actors\n");
        try {
            var dsm = Main.initializationRoutine(parent);
            dsm.become(user,password);
            dsm.listActors().stream().forEach(this::displayActor);

            return 0;
        }catch (Throwable t){
            t.printStackTrace();
            return -1;
        }
    }

    private void displayActor(Actor actor) {
        System.out.println("User: "+actor.getName());
    }

    @Command(name="follow", description="Follow another actor", defaultValueProvider = DefaultValueProvider.class)
    public int follow(
            @CommandLine.Option(names="-u", required=true, description="User Name", paramLabel="<user>") String user,
            @CommandLine.Option(
                    names="-p", required=true, description="Password",
                    paramLabel="<password>", interactive=true) String password,
            @CommandLine.Parameters String whoToFollow) {
        System.out.println("Following Actor - "+whoToFollow+"\n\n");
        try {
            var dsm = Main.initializationRoutine(parent);
            dsm.become(user,password);
            dsm.follow(whoToFollow);
            System.out.println("Successfully followed - "+whoToFollow);
            return 0;
        }catch (Throwable t){
            System.out.println("Unable to follow - "+whoToFollow);
            t.printStackTrace();
            return -1;
        }
    }

    @Command(name="show", description="Show actor information")
    public int show(@CommandLine.Parameters(index = "0", description = "Actor to show") String name){
        System.out.println("Getting actor - "+name);
        var dsm = Main.initializationRoutine(parent);
        var actor=dsm.getSimplifiedActorByName(name);

        System.out.println("[[[ ACTOR ]]]");
        System.out.println("Name:\t\t\t"+actor.name());
        System.out.println("Public-Key:\t"+actor.public_key());
        System.out.println("Hash:\t\t\t"+actor.hash());
        return 0;
    }

    @Command(name="add", description="Create a new actor", defaultValueProvider = DefaultValueProvider.class)
    public int add(
            @CommandLine.Option(
                names = "-u", required=true, description="User Name", paramLabel="<user>"
            ) String user,
            @CommandLine.Option(
                names = "-p", required=true, description="Password", paramLabel="<password>", interactive=true
            ) String password
        )  {
        System.out.println("Creating user...");

        try {
            AtomicBoolean finished = new AtomicBoolean(false);
            Consumer<Actor> c = (Actor a)->{
                finished.set(true);
            };
            var dsm = Main.initializationRoutine(parent);
            dsm.createActor(user,password,c);
            while(!finished.get());
            Thread.sleep(1000);
            System.out.println("User created: "+user);
            return 0;
        }catch (Throwable t){
            System.out.println("Unable to create user: "+t.getMessage());
            return -1;
        }
    }
}
@Command(name = "timeline",
        description = "Manage timeline",
        subcommands = {HelpCommand.class})
class TimelineCommand {
    @CommandLine.ParentCommand
    private Main parent;

    @Command(name="read", description="Monitor timeline", defaultValueProvider = DefaultValueProvider.class)
    public int read(
            @CommandLine.Option(names="-u", required=true, description="User Name", paramLabel="<user>") String user,
            @CommandLine.Option(
                    names="-p", required=false, description="Password",
                    paramLabel="<password>", interactive=true) String password){
        System.out.println("Monitoring timeline...\n\n");
        try {
            var dsm = Main.initializationRoutine(parent);

            dsm.become(user,password);
            dsm.monitorTimeline();
            System.out.println("Finished timeline monitor.\n\n");
            return 0;
        }catch (Throwable t){
            System.out.println("Unable to monitor timeline.");
            t.printStackTrace();
            return -1;
        }
    }
    @Command(name="add", description="Add a post to the timeline", defaultValueProvider = DefaultValueProvider.class)
    public int add(
            @CommandLine.Option(names = "-u", required=true, description="User Name", paramLabel="<user>") String user,
            @CommandLine.Option(
                    names = "-p", required=true, description="Password",
                    paramLabel="<password>", interactive=true) String password,
            @CommandLine.Option(
                    names = "-m", required=true, description="Message", paramLabel="<message>") String message){
        System.out.println("Posting to timeline...");
        var injector = Guice.createInjector();
        try {
            var dsm = Main.initializationRoutine(parent);

            dsm.become(user,password);
            dsm.post(message);
            System.out.println("Successfully posted message.");
            return 0;
        }catch (Throwable t){
            System.out.println("Unable to post.");
            t.printStackTrace();
            return -1;
        }
    }
}


@Command(name = "remote",
        description = "Manage remote connections",
        subcommands = {HelpCommand.class})
class RemoteCommand {
    @CommandLine.ParentCommand
    private Main parent;

    @Command(name="add", description="Add a remote connection")
    public int add(
            @CommandLine.Option(
                    names = "-a", required=true, description="Remote Address", paramLabel="<address>") String address){
        System.out.println("Attempting to add remote ["+address+"]");

        try {
            var dsm = Main.initializationRoutine(parent);
            var result = dsm.addRemoteByIPAddress(address);

            System.out.println(result);
            return 0;
        }catch (Throwable t){
            System.out.println("Unable to add remote: "+t.getMessage());
            return -1;
        }
    }
}

class DefaultValueProvider implements IDefaultValueProvider {
    public String defaultValue(ArgSpec argSpec) {
        if (argSpec.isOption()) {
            OptionSpec option = (OptionSpec) argSpec;
            if ("-p".equals(option.longestName())) {
                return System.getenv("DSM_PASSWORD");
            }
            if ("-u".equals(option.longestName())) {
                return System.getenv("DSM_USER");
            }
        }
        return null;
    }
}