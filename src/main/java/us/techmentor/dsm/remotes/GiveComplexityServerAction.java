package us.techmentor.dsm.remotes;

import com.google.inject.Inject;

import java.io.IOException;

public class GiveComplexityServerAction implements ServerAction{
    @Inject
    protected ServerOperations serverOperations;

    @Override
    public void serveClient(NetworkClientAcceptor.NetworkClient client) {
        var serverProtocol = serverOperations.getServerProtocolForClient(client);
        serverProtocol.getNextComplexityProposalFromOutgoingQueue().ifPresent(
            s-> {
                try {
                    client.output().write(s.toString().getBytes());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        );
    }
}
