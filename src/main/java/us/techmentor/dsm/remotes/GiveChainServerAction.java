package us.techmentor.dsm.remotes;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.techmentor.blockchain.persistence.ChainSerializer;

import static us.techmentor.dsm.remotes.NetworkClientAcceptor.*;

public class GiveChainServerAction implements ServerAction{

    @Inject ServerOperations serverOperations;
    @Inject ChainSerializer chainSerializer;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void serveClient(NetworkClient client) {
        try {
            var bytes =
                serverOperations
                    .getServerConnectorForClient(client)
                    .getProtocol()
                    .getNextChainFromOutgoingQueue()
                    .map(c -> chainSerializer.toBytes(c));

            logger.info("Bytes for chain: ["+bytes+"]");
            if (bytes.isPresent()) {
                client.output().write(bytes.get());
            }
        }catch (Throwable t){ logger.error(t.toString()); }
    }
}
