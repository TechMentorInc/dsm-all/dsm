package us.techmentor.dsm.person;

import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;

import org.apache.commons.codec.binary.Base64;

public class ActorCrypto {

    public static ActorCrypto getInstance(){
        return new ActorCrypto();
    }
    
    private static byte[] salt = new byte[]{0,0,0,0,0,0,0,0};
    private static byte[] IV = new byte[] { 0x01, 0x02, 0x03, 0x04,
            0x05, 0x06, 0x07, 0x08, 0x01, 0x02, 0x03, 0x04,
            0x05, 0x06, 0x07, 0x08  };

    public Cipher passwordCipher(int operationType, String password) throws Exception {
        var ps = new javax.crypto.spec.PBEParameterSpec(salt, 20, new IvParameterSpec(IV));
        var secretKey = SecretKeyFactory.getInstance("PBEWithHmacSHA256AndAES_256")
        .generateSecret(new PBEKeySpec(password.toCharArray()));

        var cipher = Cipher.getInstance("PBEWithHmacSHA256AndAES_256");
        cipher.init(operationType, secretKey, ps);
        return cipher;
    }

    public ActorKey createActorKey(String password) throws NoSuchAlgorithmException {
        var secureRandom = new SecureRandom();
        var keypairGenerator = KeyPairGenerator.getInstance("RSA");
        
        keypairGenerator.initialize(2048, secureRandom);
        var keypair =   keypairGenerator.generateKeyPair();
        
        return new ActorKey(keypair,password);
    }

    public String privateKeyEncryptBase64(ActorKey actorKey, String clearText) throws Exception{
        var encryptionCipher = Cipher.getInstance("RSA");
        encryptionCipher.init(Cipher.ENCRYPT_MODE,actorKey.getPrivateKey());
        var cipherText = encryptionCipher.doFinal(clearText.getBytes());
        return new String(Base64.encodeBase64(cipherText));
    }
    public String publicKeyDecryptFromBase64(ActorKey actorKey, String cipherText) throws Exception{
        var decryptionCipher = Cipher.getInstance("RSA");
        decryptionCipher.init(
        Cipher.DECRYPT_MODE, 
        KeyFactory
            .getInstance("RSA")
            .generatePublic( 
                new X509EncodedKeySpec(
                    actorKey.getPublicKey()
                )
            )
        );
        return new String(
            decryptionCipher.doFinal(
                Base64.decodeBase64(cipherText)
            )
        );
        
    }
}