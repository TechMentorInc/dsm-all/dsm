package us.techmentor.dsm.person;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

public class PersonFile {
    public static void save(File directory, String filename, Person person)
    throws IOException {
        var oos = 
        new ObjectOutputStream(
            new FileOutputStream(
                new File(directory, filename)
            )
        );
        oos.writeObject(person);
        oos.close();
    }

    public static Person load(Path directory, String filename) {
        try {
            var person =
                Files.list(directory)
                    .filter(Files::isRegularFile)
                    .filter( p -> p.getFileName().toString().equals(filename) )
                    .collect(Collectors.toList()).get(0).toFile();

            var oos = new ObjectInputStream(new FileInputStream(person));
            var o = oos.readObject();
            oos.close();

            return (Person)o;

        } catch (Exception e) { throw new RuntimeException(e); }
    }
}
