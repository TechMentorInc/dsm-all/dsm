package us.techmentor.dsm.person;

import java.io.Serializable;
import java.util.List;

import us.techmentor.Hash;

public class Actor implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -2229441242636553219L;
    String name;
    Hash hash;
    ActorKey actorKey;
    List<Actor> following;

    public Actor(){}
    public Actor(Person person) {
        this.name = person.name;
        this.hash = person.hash;
    }

	public Hash getHash(){
        return hash;
    }
    public String getName(){
        return name;
    }
	public ActorKey getActorKey() {
		return actorKey;
    }

    @Override public boolean equals(Object o){
        var actor = (Actor)o;
        return this.name.equals(actor.name) && this.hash.equals(actor.hash);
    }
    public List<Actor> whoAmIFollowing() {
        return following;
    }
}
