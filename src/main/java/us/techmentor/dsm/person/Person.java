package us.techmentor.dsm.person;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import us.techmentor.Hash;

import java.io.Serializable;
import java.security.Key;
import java.security.PublicKey;

public class Person implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1266868869013016239L;
    String name;
    Hash hash;
    Actor actor;
    PublicKey publicKey;

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(obj,this);
    }
    public String toString(){
        return ToStringBuilder.reflectionToString(this);
    }

    public Actor getActor() { return this.actor; }
    public void setActor(Actor actor) { this.actor = actor; }

    public String getName(){
        return name;
    }
    public Hash getHash(){
        return hash;
    }
	public Key getPublicKey() {
		return publicKey;
	}
}
