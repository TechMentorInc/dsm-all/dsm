package us.techmentor.dsm.person;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.Key;
import java.security.KeyPair;
import java.security.PublicKey;

import javax.crypto.Cipher;

public class ActorKey implements java.io.Serializable{
    /**
     *
     */
    private static final long serialVersionUID = 4037866372053602905L;
    
    private transient String password;
    byte[] privateKey;
    byte[] publicKey;
    
    public ActorKey(){}
    public ActorKey(KeyPair keyPair, String password) {
        this.password = password;
        setPrivateKey(keyPair.getPrivate());
        setPublicKey(keyPair.getPublic());
    }
    private void setPublicKey(PublicKey publicKey) {
        this.publicKey=publicKey.getEncoded();
    }
    public void setPrivateKey(Key privateKey){
        try{
            var cipher = ActorCrypto.getInstance().passwordCipher(Cipher.ENCRYPT_MODE, password);
            var bos = new ByteArrayOutputStream();
            var oos = new ObjectOutputStream(bos);
            oos.writeObject(privateKey);

            this.privateKey = cipher.doFinal(bos.toByteArray());
            oos.close();
        }catch(Exception e) { throw new RuntimeException(e); }
    }
    public void setPrivateKey(byte[] privateKey){
        this.privateKey = privateKey;
    }
	public Key getPrivateKey() {
        try{
            var cipher = ActorCrypto.getInstance().passwordCipher(Cipher.DECRYPT_MODE, password);
            var bis = new ByteArrayInputStream(cipher.doFinal(this.privateKey));
            var ois = new ObjectInputStream(bis);
            var pk = (Key)ois.readObject();
            ois.close();
            return pk;
        }catch(Exception e) { throw new RuntimeException(e); }
    }
    
    public String getPassword(){
        return this.password;
    }

	public void setPassword(String password) {
        this.password=password;
	}

    public byte[] getPublicKey() {
        return publicKey;
    }
    public byte[] getPrivateKeyData(){
        return this.privateKey;
    }
}
