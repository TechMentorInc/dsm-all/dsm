package us.techmentor.network;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import static us.techmentor.ByteArrayUtilities.prepareBytes;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BinaryOperator;


public class MessageBuilder<T>  {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    protected List<byte[]> messages = new ArrayList<byte[]>();
    protected List<Response<T>> responses = new ArrayList<Response<T>>();

    public static<T> MessageBuilder<T> createWithResult(Class<T> c) {
        if(c.equals(Boolean.class)) return (MessageBuilder<T>) new BooleanMessageBuilder();
        else return new MessageBuilder<T>();
    }

    public static final BinaryOperator FIRST_ELEMENT = (acc, elem) -> acc==null ? elem : acc;
    public static final BinaryOperator<Boolean> BOOLEAN_SUM = (acc, elem) -> {
        if (acc==null || elem==null) return elem;
        else return acc & elem;
    };

    public MessageBuilder<T> addMessage(byte message){ messages.add(new byte[] {message}); return this;}
    public MessageBuilder<T> addMessage(String message) { messages.add(message.getBytes()); return this; }
    public MessageBuilder<T> addMessage(byte[] message){ messages.add(message); return this; }

    public MessageBuilder<T> awaitResponse(int secondsToWait, String expectedValue) { return null; }
    public MessageBuilder<T> awaitResponse(int secondsToWait, Response.ResponseAction<T> responseAction){
        responses.add(new Response<T>(secondsToWait,responseAction));
        return this;
    }

    protected List<Response<T>> getResponses(){return this.responses;}
    public Optional<T> sendTCP(String address, int port, BinaryOperator<T> reduction)  {
        Socket socket = null;
        MDC.put("category","network");
        try {
            socket =  new Socket(address, port);
            var output = socket.getOutputStream();
            var input = socket.getInputStream();

            for (var m :messages){
                logger.info("Writing TCP Client Message: addr={}, port={}, msg={}", address, port, shortBytes(m));
                output.write(m);
            }

            if(getResponses().size()>0){
                T result = getResponses()
                    .stream()
                    .map(r -> {
                        try{
                            NetworkUtilities.wait(r.secondsToWait(),1000,input);
                            var available = input.available();
                            if(available==0) return null;

                            var b = new byte[available];
                            input.read(b);
                            logger.info("Reading TCP Client Message: addr={}, port={}, msg={}", address, port, shortBytes(b));

                            return r.action().apply(b,input,output);
                        }catch(Throwable throwable) {throw new RuntimeException(throwable);}
                    }).reduce(null,reduction);
                return result == null ? Optional.empty() : Optional.of(result);
            }
            return Optional.empty();
        } catch (Throwable t) {
            logger.error("Error w/TCP communication: addr={}, port={}, msg={}", address, port, t.getMessage(), t);
            throw new RuntimeException(t);
        } finally {
            MDC.clear();
            try { socket.close(); }
                catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
    }

    private String shortBytes(byte[] b) {
        return new String(b,0,Math.min(10,b.length));
    }
}
