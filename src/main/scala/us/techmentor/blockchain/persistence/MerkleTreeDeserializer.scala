package us.techmentor.blockchain.persistence

import us.techmentor.Hash
import us.techmentor.Numbers.byteArrayToInt
import us.techmentor.blockchain.fact.Fact
import us.techmentor.blockchain.merkle.{MerkleElement, MerkleTree}

import java.util.Optional

/**
 * node       := element-size{element}{children}
 * element    := {hash}:{factgroup}
 * hash       := 0-9, a-Z
 * factgroup  := fact-element-count{factelement}*
 * factelement:= fact-element-size{name}={value}
 * name,value := 0-9, a-Z
 * children   := node-count{node-size{node}}*
 */
class MerkleTreeDeserializer {

  case class DeserializationFunctions(
     node :DeserializeFunction,
     element :DeserializeFunction,
     factGroup :DeserializeFunction,
     children :DeserializeFunction,
     factElement :DeserializeFunction);

  type DeserializeFunction = (DeserializationFunctions, Array[Byte]) => MerkleElement[Fact];

  val children :DeserializeFunction =  (d,b) => {
    val count = b(0);
    if(count>2)  throw new Exception("Bad data!");

    val result = new MerkleElement[Fact]

    if(count==0) null
    else {
      var remaining = dropFirst(b)
      for (i <- 0 until count) {
        val sz = byteArrayToInt(remaining,0);
        if (i == 0) {
          val nextSlice = remaining.slice(4, sz + 4)
          result.left = Optional.of(d.node(d, nextSlice))
        } else if (i == 1) {
          val nextSlice = remaining.slice(4, sz + 4)
          result.right = Optional.of(d.node(d, nextSlice))
        }
        remaining = remaining.drop(sz + 4)
      }
      result
    }
  }

  val node :DeserializeFunction = (d,b)=>{
    val size = byteArrayToInt(b,0)
    val nslice = b.slice(4,size+4)
    val node = d.element(d,nslice)
    val childrenContent = b.drop(size+4)
    val children = d.children(d,childrenContent)

    if(children!=null) {
      node.left  = children.left;
      node.right = children.right;
      node.value = Optional.empty()
    }
    node
  }

  val element :DeserializeFunction = (d,b)=>
    new MerkleElement[Fact](){{
      val colon = b.indexOf(':')
      this.value=d.factGroup(d,b.drop(colon+1)).value
      val hashValue = b.slice(0,colon)
      val encoded = new String(hashValue);
      this.hash = Optional.of(Hash.fromString(encoded))
    }}

  val factGroup :DeserializeFunction = (d,b)=>{
    val count = b(0)
    var currentBytes = dropFirst(b);
    val returnFact = new MerkleElement[Fact](){{
      this.value = Optional.of(new Fact());
    }}
    for(i <- 1 to count){
      val sizeInBytes = currentBytes.slice(0,4);
      val size = byteArrayToInt(sizeInBytes,0);
      val elementBytes = currentBytes.drop(4).slice(0,size)
      currentBytes = currentBytes.drop(size+4)
      returnFact.value.get().getValueMap.putAll(
          d.factElement(d,elementBytes).value.get().getValueMap
      )
    }
    returnFact.value.ifPresent(v=>v.setHash(Hash.fromString(v.getValueMap.get("hash"))))
    returnFact
  }

  val factElement :DeserializeFunction = (d,b)=>{
    val parts = new String(b).split("=")
    new MerkleElement[Fact]{{
      this.value = Optional.of(new Fact(){{
        this.getValueMap.put(parts(0),parts(1))
      }})
    }}
  }

  def dropFirst[M](a :Array[M]) = a.drop(1)

  var deserializationFunctions =
    DeserializationFunctions(
      node,element,
      factGroup,children,
      factElement);

  def asTree(bytes: Array[Byte]) :MerkleTree[Fact] = {
    val root = deserializationFunctions.node(deserializationFunctions,bytes);
    new MerkleTree[Fact]{{this.setRoot(root)}}
  }
}
