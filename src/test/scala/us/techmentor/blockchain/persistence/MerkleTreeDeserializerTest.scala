package us.techmentor.blockchain.persistence

import junit.framework.Assert.{assertEquals, assertSame}
import org.apache.commons.codec.binary.Hex
import org.junit.Assert.{assertFalse, assertTrue}
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import us.techmentor.Numbers.intToByteArray
import us.techmentor.blockchain.fact.Fact
import us.techmentor.blockchain.merkle.{MerkleElement, MerkleTree}

import java.math.BigInteger
import java.util
import java.util.Optional
import java.util.concurrent.atomic.AtomicInteger

class MerkleTreeDeserializerTest extends AnyFlatSpec
  with MockFactory
  with should.Matchers {

  "asTree" should "return a tree with inigo montoya in name" in {
    val bytes = new MerkleTreeSerializer().asBytes(createMerkleTree())
    val merkleTreeDeserializer = new MerkleTreeDeserializer();
    val tree = merkleTreeDeserializer.asTree(bytes);

    assertFalse(tree.getRoot.hash.isEmpty)
    assertTrue(tree.getRoot.value.isEmpty)
    assertEquals("inigo montoya",tree.getRoot.left.get().value.get().get("name"));
  }

  "children" should "return a fact with the sub-facts on them (the left/right links)" in{
    val bytes = Array.concat(Array[Byte](2),intToByteArray(5),Array[Byte](1,2,3,4,5),intToByteArray(5),Array[Byte](6,7,8,9,0));
    val nodeCalls :AtomicInteger = new AtomicInteger(0);
    val merkleTreeDeserializer = new MerkleTreeDeserializer
    val returnFacts =
      Array(
        new MerkleElement[Fact](),
        new MerkleElement[Fact]())
    val testBytes = Array(
      Array[Byte](1,2,3,4,5),
      Array[Byte](6,7,8,9,0)
    )

    val node: merkleTreeDeserializer.DeserializeFunction =
      (d,b) => {
        val instance = nodeCalls.getAndIncrement();
        if(!b.sameElements(testBytes(instance)))
          throw new Exception("Bytes are wrong!")
        returnFacts(instance)
      }

    val d =
      merkleTreeDeserializer
        .DeserializationFunctions(
          node,null,null,null,null);

    val result = merkleTreeDeserializer
      .deserializationFunctions
      .children(d,bytes)
    assertEquals(2,nodeCalls.get());
    assertSame(returnFacts(0),result.left.get())
    assertSame(returnFacts(1),result.right.get())
  }
  "children" should "have no more than two children" in{

  }
  "factelement" should "return a fact with a single fact element on it" in {
    val bytes = "keyname=value".getBytes()
    val merkleTreeDeserializer = new MerkleTreeDeserializer
    val result = merkleTreeDeserializer.deserializationFunctions.factElement(null,bytes);

    assertEquals("value",result.value.get().getValueMap().get("keyname"))
  }
  "factgroup" should "separate the count, then call fact element that many times" in {
    val factElementCalls :AtomicInteger = new AtomicInteger(0);

    val merkleTreeDeserializer = new MerkleTreeDeserializer
    val factElement: merkleTreeDeserializer.DeserializeFunction =
    (f,bytes) => {
      factElementCalls.incrementAndGet()
      new MerkleElement[Fact](){{
        this.value = Optional.of(new Fact());
        this.value.get().getValueMap.put("name"+factElementCalls.get(),"value");
      }}
    };

    val count :Byte = 2
    val size :Array[Byte] = intToByteArray(3)
    val bytes = Array.concat(Array[Byte](count),size,Array[Byte](1,2,3),size,Array[Byte](1,3,4))
    val deserializationFunctions =
      merkleTreeDeserializer
        .DeserializationFunctions(
          null,null,null,null,factElement);

    val resultFact = merkleTreeDeserializer
      .deserializationFunctions
      .factGroup(deserializationFunctions,bytes)

    assertEquals(2,factElementCalls.get())
    assertEquals(2,resultFact.value.get().getValueMap.size());
  }

  "element" should "return an element with hash and a map of stuff" in {
    val resultFact = new Fact();

    val merkleTreeDeserializer = new MerkleTreeDeserializer
    val factGroup: merkleTreeDeserializer.DeserializeFunction =
      (f,b) => {
        if(!b.sameElements("6789".getBytes()))
          throw new RuntimeException("Test Failure - Bytes Wrong")
        new MerkleElement[Fact](){{this.value = Optional.of(resultFact)}}
      };

    val deserializationFunctions =
      merkleTreeDeserializer
        .DeserializationFunctions(
          null,null,factGroup,null,null);

    val result =
    merkleTreeDeserializer
      .deserializationFunctions
      .element(deserializationFunctions,"12345:6789".getBytes());

    assertSame(resultFact, result.value.get())

    assertEquals(BigInteger.valueOf(74565),result.hash.get().asBigInteger())
  }

  "node" should "call element with n bytes and children with the remainder" in {
    val merkleTreeDeserializer = new MerkleTreeDeserializer
    val resultTree = new MerkleElement[Fact]();
    val resultLeft = Optional.of(new MerkleElement[Fact]())
    val resultRight = Optional.of(new MerkleElement[Fact]())

    val elementParser: merkleTreeDeserializer.DeserializeFunction =
      (f,bytes) => {
        if(!bytes.sameElements(Array[Byte](1,2,3)))
          throw new RuntimeException("Test Failure - Bytes Wrong")
        resultTree
      };

    val childrenParser: merkleTreeDeserializer.DeserializeFunction =
      (f,bytes) => {
        if(!bytes.sameElements(Array[Byte](4,5,6,7,8,9)))
          throw new RuntimeException("Test Failure - Bytes Wrong")
        val top = new MerkleElement[Fact]();
        top.left = resultLeft
        top.right = resultRight
        top
      };

    val bytes = Array[Byte](0,0,0,3,1,2,3,4,5,6,7,8,9);
    val deserializationFunctions =
      merkleTreeDeserializer.DeserializationFunctions(
        null,
        elementParser,
        null,
        childrenParser,
        null);

    val resultNode = merkleTreeDeserializer
      .deserializationFunctions
      .node(deserializationFunctions,bytes)

    assertEquals(resultTree,resultNode);
    assertEquals(resultTree.left, resultLeft);
    assertEquals(resultTree.right, resultRight);
  }

  def createMerkleTree() =
    new MerkleTree[Fact](){{
      add(new Fact(new util.HashMap[String,String](){{
        this.put("name","inigo montoya")
      }}))
      add(new Fact(new util.HashMap[String,String](){{
        this.put("name","westley")
      }}))
    }}
}
