package us.techmentor;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ByteArrayUtilitiesTest {
    @Test
    public void testSplit(){
        String theTestString = "Test,Test,123";

        var byteArrayUtilities = new ByteArrayUtilities();
        var results = byteArrayUtilities.split(theTestString.getBytes(),(byte)',');

        assertEquals(3, results.length);
        assertEquals("123",new String(results[2]));
        assertEquals("Test",new String(results[1]));
        assertEquals("Test",new String(results[0]));
    }
}
