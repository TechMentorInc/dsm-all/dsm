package us.techmentor.network;

import org.junit.Test;
import scala.math.Equiv;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MessageBuilderTest {
    @Test
    public void testCreate(){
        var messageBuilder = MessageBuilder.createWithResult(String.class);
        assertTrue(messageBuilder instanceof MessageBuilder);
    }

    @Test
    public void testAddMessage(){
        var messageBuilder = MessageBuilder.createWithResult(Boolean.class);
        messageBuilder.addMessage("CONNECT");
        assertTrue(Arrays.equals((byte[]) messageBuilder.messages.get(0),"CONNECT".getBytes()));
    }

    @Test
    public void testAssembleBytes(){
        var messageBuilder =
        MessageBuilder
            .createWithResult(Boolean.class)
            .addMessage("tm")
            .addMessage("tm2");

        assertEquals(
            new String(messageBuilder.messages.get(1)).charAt(2),
            '2');
    }
}
