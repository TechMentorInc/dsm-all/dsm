package us.techmentor.blockchain.persistence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.inject.Guice;

import org.junit.Test;
import us.techmentor.blockchain.BlockPreparation;
import us.techmentor.blockchain.Blockchain;
import us.techmentor.blockchain.FactOperations;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.fact.FactQuery;

public class ChainSerializerTest {
    @Test
    public void testChainSerializer() throws InterruptedException{
        var injector = Guice.createInjector();
        var b1 = injector.getInstance(Blockchain.class);
        var b1FactOperations = injector.getInstance(FactOperations.class);

        var finished = new AtomicInteger(0);
        b1FactOperations.saveFact(new Fact(Map.of("id","123")),b->finished.addAndGet(b.getFacts().asList().size()));
        while(finished.get()!=1);

        b1FactOperations.saveFact(new Fact(Map.of("id","456")),b->finished.addAndGet(b.getFacts().asList().size()));
        while(finished.get()!=2);

        var chainSerializer = injector.getInstance(ChainSerializer.class);
        var results = chainSerializer.serializeChain(b1);

        assertEquals(2, results.size());
        assertTrue(new String(results.get(0).getData()).contains("123"));
        assertTrue(new String(results.get(1).getData()).contains("456"));
        assertTrue(results.get(0).getBlock().getFacts().asList().get(0).get("id").equals("123"));
        assertTrue(results.get(1).getBlock().getFacts().asList().get(0).get("id").equals("456"));
        
    }
    @Test
    public void testChainDeserializer() throws Exception{
        var injector = Guice.createInjector();
        var b1 = injector.getInstance(Blockchain.class);
        var b1FactOperations = injector.getInstance(FactOperations.class);

        var finished = new AtomicBoolean(false);
        b1FactOperations.saveFact(new Fact(Map.of("id","123","message","hello world")),b->finished.set(true));
        while(!finished.get());

        finished.set(false);
        b1FactOperations.saveFact(new Fact(Map.of("id","456")),b->finished.set(true));
        while(!finished.get());

        var chainSerializer = injector.getInstance(ChainSerializer.class);
        var serialized = chainSerializer.serializeChain(b1);
        var deserialized = chainSerializer.deserializeChain(serialized);

        var testQuery = Map.<String, FactQuery.Truth>of("id",new FactQuery.StringTruth("123"));
        var resultFact = deserialized.locateFacts(new FactQuery(testQuery));

        assertEquals(resultFact.get(0).get("message").toString(), "hello world");
    }

    @Test
    public void testCommasShouldBeAllowedInNameAndValue(){
        // block serializer splits on commas - which messes things up
    }

    @Test
    public void toAndFromBytes() throws InterruptedException {
        var injector = Guice.createInjector();
        var b1 = injector.getInstance(Blockchain.class);
        var b1FactOperations = injector.getInstance(FactOperations.class);

        var finished = new AtomicBoolean(false);
        b1FactOperations.saveFact(new Fact(Map.of("id","123","message","hello world")),b->finished.set(true));
        while(!finished.get());

        var chainSerializer = injector.getInstance(ChainSerializer.class);

        var chain = chainSerializer.fromBytes(chainSerializer.toBytes(b1.getChain()));
        var testQuery = Map.<String, FactQuery.Truth>of("id",new FactQuery.StringTruth("123"));
        var resultFact = chain.locateFacts(new FactQuery(testQuery));

        assertEquals("hello world",resultFact.get(0).get("message"));
    }
}
