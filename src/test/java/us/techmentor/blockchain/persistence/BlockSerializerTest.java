package us.techmentor.blockchain.persistence;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.math.BigInteger;

import org.junit.Test;

import us.techmentor.ByteArrayUtilities;
import us.techmentor.Hash;
import us.techmentor.blockchain.Block;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.merkle.MerkleTree;

public class BlockSerializerTest {
    @Test
    public void blockAsDataTest(){
        var tree = new MerkleTree<Fact>(){{
            this.add(new Fact());
        }};
        var block =(Block) (new Block(){{
            this.facts = tree;
            this.hash = new Hash(){{this.value="1234";}};
            this.previousBlockHash = new Hash(){{this.value="4567";}};
            this.nonce = BigInteger.valueOf(1000000);
        }});
        
        var merkleTreeSerializerMock = mock(MerkleTreeSerializer.class);
        var blockSerializer = new BlockSerializer();
        blockSerializer.merkleTreeSerializer = merkleTreeSerializerMock;

        var stringResult = new String(blockSerializer.asBytes(block));

        assertTrue(stringResult.contains("1234"));
        assertTrue(stringResult.contains("4567"));
        assertTrue(stringResult.contains("1000000"));

        verify(merkleTreeSerializerMock).asBytes(tree);
    }

    @Test
    public void dataAsBlockTest(){
        var tree = new MerkleTree<Fact>(){{
            this.add(new Fact());
        }};
        var block =(Block) (new Block(){{
            this.facts = tree;
            this.hash = new Hash(){{this.value="1234";}};
            this.previousBlockHash = new Hash(){{this.value="4567";}};
            this.nonce = BigInteger.valueOf(1000000);
        }});

        var mockTree = new MerkleTree<Fact>();
        var merkleTreeDeserializerMock = mock(MerkleTreeDeserializer.class);
        var merkleTreeSerializerMock = mock(MerkleTreeSerializer.class);

        var blockSerializer = new BlockSerializer();
        var byteArrayUtilities = new ByteArrayUtilities();
        blockSerializer.merkleTreeDeserializer = merkleTreeDeserializerMock;
        blockSerializer.merkleTreeSerializer = merkleTreeSerializerMock;
        blockSerializer.byteArrayUtilities = byteArrayUtilities;

        var serializedBytes = blockSerializer.asBytes(block);
        when(merkleTreeDeserializerMock.asTree(byteArrayUtilities.split(serializedBytes,(byte)',')[4])).thenReturn(mockTree);

        var result = blockSerializer.asBlock(serializedBytes);
        assertSame(mockTree, result.getFacts());

        assertEquals("1234",result.getHash().getValue());
        assertEquals("4567",result.getPreviousBlockHash().getValue());
        assertEquals(BigInteger.valueOf(1000000),result.getNonce());
    }
    @Test
    public void dataAsBlockWithNullPreviousBlockTest(){
        var tree = new MerkleTree<Fact>(){{
            this.add(new Fact());
        }};
        var block =(Block) (new Block(){{
            this.facts = tree;
            this.hash = new Hash(){{this.value="1234";}};
            this.previousBlockHash = null;
            this.nonce = BigInteger.valueOf(1000000);
        }});

        var mockTree = new MerkleTree<Fact>();
        var merkleTreeDeserializerMock = mock(MerkleTreeDeserializer.class);
        var merkleTreeSerializerMock = mock(MerkleTreeSerializer.class);

        var blockSerializer = new BlockSerializer();
        var byteArrayUtilities = new ByteArrayUtilities();
        blockSerializer.merkleTreeDeserializer = merkleTreeDeserializerMock;
        blockSerializer.merkleTreeSerializer = merkleTreeSerializerMock;
        blockSerializer.byteArrayUtilities = byteArrayUtilities;

        var serializedBytes = blockSerializer.asBytes(block);
        when(merkleTreeDeserializerMock.asTree(byteArrayUtilities.split(serializedBytes,(byte)',')[4])).thenReturn(mockTree);

        var result = blockSerializer.asBlock(serializedBytes);
        assertSame(mockTree, result.getFacts());

        assertEquals("1234",result.getHash().getValue());
        assertEquals(null,result.getPreviousBlockHash());
        assertEquals(BigInteger.valueOf(1000000),result.getNonce());
    }
}
