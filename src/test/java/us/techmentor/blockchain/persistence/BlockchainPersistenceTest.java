package us.techmentor.blockchain.persistence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;

import com.google.inject.Injector;
import org.junit.Test;

import us.techmentor.blockchain.*;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.dsm.metadata.MetaDataOperations;

public class BlockchainPersistenceTest {
    private void forceInjectorsToUseTempDirectory(Injector[] injectors){
        for(Injector injector : injectors){
            var metaDataOperations = injector.getInstance(MetaDataOperations.class);
            metaDataOperations.forceTempHomeDirectory();
        }
    }


    @Test 
    public void testSaveChain(){
        var chainSerializerMock = mock(ChainSerializer.class);
        var diskPersistenceMock = mock(DiskPersistence.class);

        var blockchain = new Blockchain();
        var blocks = Arrays.asList(new PreparedBlock());
        
        when(chainSerializerMock.serializeChain(blockchain)).thenReturn(blocks);
        var blockchainPersistence = new BlockchainPersistence();
        blockchainPersistence.chainSerializer=chainSerializerMock;
        blockchainPersistence.diskPersistence=diskPersistenceMock;
        blockchainPersistence.save(blockchain);

        verify(diskPersistenceMock).commit(blocks);
    }

    @Test
    public void testLoadChain(){
        var diskPersistenceMock = mock(DiskPersistence.class);
        var blocks = Arrays.asList(new PreparedBlock());
        when(diskPersistenceMock.retrieve()).thenReturn(blocks);

        Blockchain blockchain=new Blockchain();
        var chainSerializerMock = mock(ChainSerializer.class);
        when(chainSerializerMock.deserializeChain(any())).thenReturn(blockchain.getChain());

        var blockchainPersistence = new BlockchainPersistence();
        blockchainPersistence.chainSerializer=chainSerializerMock;
        blockchainPersistence.diskPersistence=diskPersistenceMock;

        var result=blockchainPersistence.load();

        assertSame(result,blockchain.getChain());

    }
    
    @Test
    public void testSaveAndLoadChain() throws InterruptedException{
        var injector1 = Guice.createInjector();
        var injector2 = Guice.createInjector();
        forceInjectorsToUseTempDirectory(new Injector[]{injector1, injector2});

        var b1 = injector1.getInstance(Blockchain.class);
        var b1FactOperations = injector1.getInstance(FactOperations.class);

        var finished = new AtomicBoolean(false);
        b1FactOperations.saveFact(new Fact(Map.of("id","123")),b->finished.set(true));
        while(!finished.get());

        finished.set(false);
        b1FactOperations.saveFact(new Fact(Map.of("id","456")),b->finished.set(true));
        while(!finished.get());
        
        var blockchainPersistence =
                injector2.getInstance(BlockchainPersistence.class);
        blockchainPersistence.save(b1);

        var b2 = injector2.getInstance(Blockchain.class);
        var b2FactOperations = injector2.getInstance(FactOperations.class);

        var b2chain = blockchainPersistence.load();
        b2.setChain(b2chain);

        finished.set(false);
        b2FactOperations.saveFact(new Fact(Map.of("id","789")),b->finished.set(true));
        while(!finished.get());

        var chainComparator = new ChainComparator();
        var result = chainComparator.compare(b1.getChain(), b2.getChain());
        assertEquals(1,result.lastCommonIndex);
    }
}
