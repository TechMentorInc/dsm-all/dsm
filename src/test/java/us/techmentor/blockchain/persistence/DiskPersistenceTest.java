package us.techmentor.blockchain.persistence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import us.techmentor.Hash;
import us.techmentor.blockchain.Block;
import us.techmentor.dsm.metadata.FileMetaDataOperations;
import us.techmentor.dsm.metadata.MetaDataOperations;
import us.techmentor.dsm.metadata.IdOperations;

public class DiskPersistenceTest {
    @Test
    public void testCommit(){
        var sb1 = new PreparedBlock(){{
            this.data="it's amazing how much can be done if we're always doing".getBytes();
            this.block= new Block(){{
                this.hash=new Hash(){{
                    this.value = "1234";
                }};
            }};
        }};
        var sb2 = new PreparedBlock(){{
            this.data="a foolish consistency is the hobgoblin of small minds".getBytes();
            this.block= new Block(){{
                this.hash=new Hash(){{
                    this.value = "4567";
                }};
            }};
        }};
        var diskPersistence = new DiskPersistence();
        diskPersistence.metaDataOperations =new MetaDataOperations(){{
            this.fileMetaDataOperations = new FileMetaDataOperations(){{
                this.idOperations = new IdOperations();
            }};
        }};
        diskPersistence.metaDataOperations.forceTempHomeDirectory();

        diskPersistence.commit(Arrays.asList(sb1,sb2));

        var result = diskPersistence.retrieve();
        assertEquals(2, result.size());

        var first = new String(result.get(0).data);
        var second = new String(result.get(1).data);
        System.out.println(">> result.get(0).data="+first);
        System.out.println(">> result.get(1).data="+second);

        assertTrue(first.contains("amazing") || second.contains("amazing"));
        assertTrue(first.contains("consistency") || second.contains("consistency"));
    }
}
