package us.techmentor.blockchain.speed;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Test;
import us.techmentor.blockchain.BlockPreparation;
import us.techmentor.blockchain.speed.complexity.ComplexityDefinition;
import us.techmentor.dsm.DSM;
import us.techmentor.dsm.metadata.FileMetaDataOperations;
import us.techmentor.dsm.metadata.MetaDataOperations;

import java.math.BigInteger;

import static org.mockito.Mockito.*;

public class BlockSpeedAdaptationTest {
    private void forceInjectorsToUseTempDirectory(Injector[] injectors){
        for(Injector injector : injectors){
            var metaDataOperations = injector.getInstance(MetaDataOperations.class);
            metaDataOperations.forceTempHomeDirectory();
        }
    }

    @Test
    public void startTest(){
        var blockSpeedAdaptationSpy = spy(BlockSpeedAdaptation.class);
        var statisticsMock = mock(Statistics.class);
        blockSpeedAdaptationSpy.statistics = statisticsMock;
        blockSpeedAdaptationSpy.start();
        verify(blockSpeedAdaptationSpy,timeout(3000).times(2)).check();
        blockSpeedAdaptationSpy.stop();
    }

    @Test
    public void checkNoChangeTest(){
        var blockSpeedAdaptationSpy = spy(BlockSpeedAdaptation.class);

        var statisticsMock = mock(Statistics.class);
        blockSpeedAdaptationSpy.statistics = statisticsMock;

        blockSpeedAdaptationSpy.check();

        verify(statisticsMock).speedWithinTolerance();
        verify(blockSpeedAdaptationSpy,never()).increaseSpeed();
        verify(blockSpeedAdaptationSpy,never()).decreaseSpeed();
    }
    @Test
    public void checkAboveTest(){
        var blockSpeedAdaptationSpy = spy(BlockSpeedAdaptation.class);
        var statisticsMock = mock(Statistics.class);
        blockSpeedAdaptationSpy.statistics = statisticsMock;

        doNothing().when(blockSpeedAdaptationSpy).increaseSpeed();
        doNothing().when(blockSpeedAdaptationSpy).decreaseSpeed();
        when(statisticsMock.speedWithinTolerance()).thenReturn(2.0);
        blockSpeedAdaptationSpy.check();

        verify(blockSpeedAdaptationSpy,never()).increaseSpeed();
        verify(blockSpeedAdaptationSpy,times(1)).decreaseSpeed();
    }

    @Test
    public void basicProposalAndAcceptance_ClientToServer() throws InterruptedException {
        var blockPreparationLocalMock = mock(BlockPreparation.class);
        when(blockPreparationLocalMock.getComplexity()).thenReturn(new ComplexityDefinition(BigInteger.ONE));
        var blockPreparationRemoteMock = mock(BlockPreparation.class);
        when(blockPreparationRemoteMock.getComplexity()).thenReturn(new ComplexityDefinition(BigInteger.ONE));

        var statisticsMock = mock(Statistics.class);
        when(statisticsMock.meanRelativeToTarget()).thenReturn(10000D);

        var injectorRemote = Guice.createInjector(
                new AbstractModule() {
                    @Override
                    protected void configure() {
                        bind(BlockPreparation.class).toInstance(blockPreparationRemoteMock);
                        bind(Statistics.class).toInstance(statisticsMock);
                    }
                });
        var injectorLocal = Guice.createInjector(
                new AbstractModule() {
                    @Override
                    protected void configure() {
                        bind(BlockPreparation.class).toInstance(blockPreparationLocalMock);
                        bind(Statistics.class).toInstance(statisticsMock);
                    }
                });

        forceInjectorsToUseTempDirectory(new Injector[]{injectorLocal, injectorRemote});
        var dsmLocal = injectorLocal.getInstance(DSM.class);
        var dsmRemote = injectorRemote.getInstance(DSM.class);
        try {
            dsmRemote.serverOperations.listen();
            Thread.sleep(4000);

            dsmLocal.addRemoteByIPAddress("127.0.0.1");
            dsmLocal.heartbeat.remoteOperations.createRemoteConnectors(dsmLocal.heartbeat.remoteOperations.getAllRemotes());
            Thread.sleep(6000);

            dsmLocal.blockSpeedAdaptation.increaseSpeed();

            var c = new ComplexityDefinition(new BigInteger("1f",16));
            verify(blockPreparationLocalMock,timeout(4000)).setComplexity(c);
            verify(blockPreparationRemoteMock,timeout(4000)).setComplexity(c);
        }finally {
            dsmLocal.stop();
            dsmRemote.stop();
        }
    }

    @Test
    public void targetManipulationLearningTest(){
        var quickTarget = new BigInteger("000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF",16);
        System.out.println(String.format("quick: %1$35x",quickTarget).replace(' ','0'));
        var qt2 = quickTarget.shiftRight(8);
        System.out.println(String.format("quick: %1$35x",qt2).replace(' ','0'));
        var qt3 = quickTarget.shiftLeft(8).or(BigInteger.valueOf(0xff));
        System.out.println(String.format("quick: %1$35x",qt3).replace(' ','0'));
    }

    @Test
    public void basicProposalAndAcceptance_ServerToClient() throws InterruptedException {
        var blockPreparationLocalMock = mock(BlockPreparation.class);
        when(blockPreparationLocalMock.getComplexity()).thenReturn(new ComplexityDefinition(BigInteger.ONE));
        var blockPreparationRemoteMock = mock(BlockPreparation.class);
        when(blockPreparationRemoteMock.getComplexity()).thenReturn(new ComplexityDefinition(BigInteger.ONE));

        var statisticsMock = mock(Statistics.class);
        when(statisticsMock.meanRelativeToTarget()).thenReturn(10000D);

        var injectorRemote = Guice.createInjector(
                new AbstractModule() {
                    @Override
                    protected void configure() {
                        bind(BlockPreparation.class).toInstance(blockPreparationRemoteMock);
                        bind(Statistics.class).toInstance(statisticsMock);
                    }
                });
        var injectorLocal = Guice.createInjector(
                new AbstractModule() {
                    @Override
                    protected void configure() {
                        bind(BlockPreparation.class).toInstance(blockPreparationLocalMock);
                        bind(Statistics.class).toInstance(statisticsMock);
                    }
                });
        forceInjectorsToUseTempDirectory(new Injector[]{injectorLocal, injectorRemote});
        var dsmLocal = injectorLocal.getInstance(DSM.class);
        var dsmRemote = injectorRemote.getInstance(DSM.class);
        try {
            dsmRemote.serverOperations.listen();
            Thread.sleep(4000);

            dsmLocal.addRemoteByIPAddress("127.0.0.1");
            dsmLocal.heartbeat.remoteOperations.createRemoteConnectors(dsmLocal.heartbeat.remoteOperations.getAllRemotes());
            Thread.sleep(6000);

            dsmRemote.blockSpeedAdaptation.increaseSpeed();

            var c = new ComplexityDefinition(new BigInteger("1f",16));
            verify(blockPreparationLocalMock,timeout(4000)).setComplexity(c);
            verify(blockPreparationRemoteMock,timeout(4000)).setComplexity(c);
        }finally {
            dsmLocal.stop();
            dsmRemote.stop();
        }
    }

    public void basicProposalAndAcceptance_ClientToServerToClient() throws InterruptedException {
        // this needs to be a full integration test -> can't have two clients from same IP
    }
    public void failsAcceptanceThreshold_C2S2C(){

    }
    @Test
    public void decreaseSpeed_ClientToServer() throws InterruptedException {
        var blockPreparationLocalMock = mock(BlockPreparation.class);
        when(blockPreparationLocalMock.getComplexity()).thenReturn(new ComplexityDefinition(new BigInteger("FFFFFFFF",16)));

        var blockPreparationRemoteMock = mock(BlockPreparation.class);
        when(blockPreparationRemoteMock.getComplexity()).thenReturn(new ComplexityDefinition(new BigInteger("FFFFFFFF",16)));

        var statisticsMock = mock(Statistics.class);
        when(statisticsMock.meanRelativeToTarget()).thenReturn(-10000D);
        var injectorRemote = Guice.createInjector(
                new AbstractModule() {
                    @Override
                    protected void configure() {

                        bind(BlockPreparation.class).toInstance(blockPreparationRemoteMock);
                        bind(Statistics.class).toInstance(statisticsMock);
                    }
                });
        var injectorLocal = Guice.createInjector(
                new AbstractModule() {
                    @Override
                    protected void configure() {

                        bind(BlockPreparation.class).toInstance(blockPreparationLocalMock);
                        bind(Statistics.class).toInstance(statisticsMock);
                    }
                });

        forceInjectorsToUseTempDirectory(new Injector[]{injectorLocal, injectorRemote});
        var dsmLocal = injectorLocal.getInstance(DSM.class);
        var dsmRemote = injectorRemote.getInstance(DSM.class);
        try {
            dsmRemote.serverOperations.listen();
            Thread.sleep(4000);

            dsmLocal.addRemoteByIPAddress("127.0.0.1");
            dsmLocal.heartbeat.remoteOperations.createRemoteConnectors(dsmLocal.heartbeat.remoteOperations.getAllRemotes());
            Thread.sleep(6000);

            dsmLocal.blockSpeedAdaptation.decreaseSpeed();

            var c = new ComplexityDefinition(new BigInteger("fffffff",16));
            verify(blockPreparationLocalMock,timeout(6000)).setComplexity(c);
            verify(blockPreparationRemoteMock,timeout(6000)).setComplexity(c);
        }finally {
            dsmLocal.stop();
            dsmRemote.stop();
        }
    }

    @Test
    public void failsStatEvaluationOnRemote_ClientToServer() throws InterruptedException {
        var blockPreparationLocalMock = mock(BlockPreparation.class);
        when(blockPreparationLocalMock.getComplexity()).thenReturn(new ComplexityDefinition(new BigInteger("FFFFFFFF",16)));
        var blockPreparationRemoteMock = mock(BlockPreparation.class);
        when(blockPreparationRemoteMock.getComplexity()).thenReturn(new ComplexityDefinition(new BigInteger("FFFFFFFF",16)));
        var statisticsMock = mock(Statistics.class);
        when(statisticsMock.meanRelativeToTarget()).thenReturn(10000D);
        var injectorRemote = Guice.createInjector(
                new AbstractModule() {
                    @Override
                    protected void configure() {

                        bind(BlockPreparation.class).toInstance(blockPreparationRemoteMock);
                        bind(Statistics.class).toInstance(statisticsMock);
                    }
                });
        var injectorLocal = Guice.createInjector(
                new AbstractModule() {
                    @Override
                    protected void configure() {
                        bind(BlockPreparation.class).toInstance(blockPreparationLocalMock);
                        bind(Statistics.class).toInstance(statisticsMock);
                    }
                });

        forceInjectorsToUseTempDirectory(new Injector[]{injectorLocal, injectorRemote});
        var dsmLocal = injectorLocal.getInstance(DSM.class);
        var dsmRemote = injectorRemote.getInstance(DSM.class);
        try {
            dsmRemote.serverOperations.listen();
            Thread.sleep(4000);

            dsmLocal.addRemoteByIPAddress("127.0.0.1");
            dsmLocal.heartbeat.remoteOperations.createRemoteConnectors(dsmLocal.heartbeat.remoteOperations.getAllRemotes());
            Thread.sleep(6000);

            dsmLocal.blockSpeedAdaptation.decreaseSpeed();

            var c = new ComplexityDefinition(new BigInteger("fffffff",16));
            verify(blockPreparationLocalMock,never()).setComplexity(c);
            verify(blockPreparationRemoteMock,never()).setComplexity(c);
        }finally {
            dsmLocal.stop();
            dsmRemote.stop();
        }
    }
}
