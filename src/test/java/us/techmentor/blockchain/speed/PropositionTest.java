package us.techmentor.blockchain.speed;

import com.google.inject.Guice;
import org.junit.Test;
import us.techmentor.blockchain.BlockPreparation;
import us.techmentor.blockchain.Blockchain;
import us.techmentor.blockchain.speed.complexity.Complexity;
import us.techmentor.blockchain.speed.complexity.ComplexityDefinition;
import us.techmentor.blockchain.speed.complexity.ComplexityProposal;

import java.math.BigInteger;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class PropositionTest {
    private static int SUCCESS = 1;
    private static int FAIL = 2;

    @Test
    public void increaseSpeedTest(){
        var proposition = new Proposition();
        var connectorMock = mock(SpeedAdaptingConnector.class);
        var complexityMock = mock(Complexity.class);
        var blockPreparationMock = mock(BlockPreparation.class);
        var complexityDefinition = new ComplexityDefinition(BigInteger.valueOf(0));
        var complexityProposal = new ComplexityProposal(null,null,null,null);

        when(blockPreparationMock.getComplexity()).thenReturn(complexityDefinition);
        when(complexityMock.decreasedComplexity(complexityDefinition)).thenReturn(complexityDefinition);
        when(complexityMock.createNoActionProposal(complexityDefinition)).thenReturn(complexityProposal);

        doAnswer((invocation)->{
            invocation.getArgument(SUCCESS,Runnable.class).run();
            return null;
        }).when(connectorMock).proposeComplexity(
                eq(complexityProposal),
            any(Runnable.class),
            any(Runnable.class)
        );

        var proposableConnectorManagerMock = mock(SpeedAdaptingConnectorManager.class);
        when(proposableConnectorManagerMock.getSpeedAdaptingConnectors()).thenReturn(
                new SpeedAdaptingConnector[]{connectorMock}
        );
        proposition.blockPreparation=blockPreparationMock;
        proposition.complexity=complexityMock;
        proposition.speedAdaptingConnectorManager=proposableConnectorManagerMock;

        var result = proposition.increaseSpeed();
        assert(result.isPresent());
        verify(connectorMock).proposeComplexity(eq(complexityProposal),any(Runnable.class),any(Runnable.class));
    }
    @Test
    public void decreaseSpeedTest(){
        var proposition = new Proposition();
        var connectorMock = mock(SpeedAdaptingConnector.class);
        var complexityMock = mock(Complexity.class);
        var blockPreparationMock = mock(BlockPreparation.class);
        var complexityDefinition = new ComplexityDefinition(BigInteger.valueOf(0));
        var complexityProposal = new ComplexityProposal(null,null,null,null);

        when(blockPreparationMock.getComplexity()).thenReturn(complexityDefinition);
        when(complexityMock.increasedComplexity(complexityDefinition)).thenReturn(complexityDefinition);
        when(complexityMock.createNoActionProposal(complexityDefinition)).thenReturn(complexityProposal);

        doAnswer((invocation)->{
            invocation.getArgument(SUCCESS,Runnable.class).run();
            return null;
        }).when(connectorMock).proposeComplexity(
                eq(complexityProposal),
                any(Runnable.class),
                any(Runnable.class)
        );

        var proposableConnectorManagerMock = mock(SpeedAdaptingConnectorManager.class);
        when(proposableConnectorManagerMock.getSpeedAdaptingConnectors()).thenReturn(
                new SpeedAdaptingConnector[]{connectorMock}
        );
        proposition.blockPreparation=blockPreparationMock;
        proposition.complexity=complexityMock;
        proposition.speedAdaptingConnectorManager=proposableConnectorManagerMock;

        var result = proposition.decreaseSpeed();
        assert(result.isPresent());
        verify(connectorMock).proposeComplexity(eq(complexityProposal),any(Runnable.class),any(Runnable.class));
    }

    @Test
    public void learningInjectInterfaceTest(){
        var injector = Guice.createInjector();
        var proposition = injector.getInstance(Proposition.class);

        assert(proposition.speedAdaptingConnectorManager instanceof Blockchain);
    }
}
