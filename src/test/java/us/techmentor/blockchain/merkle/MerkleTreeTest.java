package us.techmentor.blockchain.merkle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.Test;

import us.techmentor.Hash;

public class MerkleTreeTest {
    @Test
    public void asListTest(){
        var merkleTree = new MerkleTree<String>();
        merkleTree.addAll(List.of("1","2","3"));
        
        assertTrue(merkleTree.asList().contains("1"));
        assertTrue(merkleTree.asList().contains("2"));
        assertTrue(merkleTree.asList().contains("3"));
    }

    @Test
    public void addTest(){
        var merkleTree = new MerkleTree<String>();

        merkleTree.add("1");
        merkleTree.add("2");

        var merkleTreeRoot = merkleTree.getRoot();

        assertTrue(merkleTreeRoot.left.get().value.get().equals("1"));
        assertTrue(merkleTreeRoot.right.get().value.get().equals("2"));
    }

    @Test
    public void addThreeLevelsTest(){
        var merkleTree = new MerkleTree<String>();

        for(int i=1; i<=4; i++){
            merkleTree.add(""+i);
        }
        
        var merkleTreeRoot = merkleTree.getRoot();
        
        assertTrue(merkleTreeRoot.left.get().left.get().value.get().equals("1"));
        assertTrue(merkleTreeRoot.left.get().right.get().value.get().equals("3"));
        assertTrue(merkleTreeRoot.right.get().left.get().value.get().equals("2"));
        assertTrue(merkleTreeRoot.right.get().right.get().value.get().equals("4"));
    }

    @Test
    public void addAndProperlyAssignedHashTest(){
        var merkleTree = new MerkleTree<String>();

        String s1 = "1";
        String s2 = "2";
        merkleTree.add(s1);
        merkleTree.add(s2);

        var merkleTreeRoot = merkleTree.getRoot();

        var h1 = Hash.create(s1);
        var h2 = Hash.create(s2);

        var roothash = Hash.create("\""+h1.getValue()+"\":\""+h2.getValue()+"\"");
        assertEquals(roothash,merkleTreeRoot.hash.get());
    }


    @Test
    public void findOpenSlotTestRoot(){
        var merkleTree = new MerkleTree<String>();
        MerkleElement<String> merkleElement =
            merkleTree.findOpenSlot();

        assertEquals(merkleTree.root, merkleElement);
    }

    @Test
    public void findOpenSlotTestOneLevel(){
        var merkleTree = new MerkleTree<String>();
        merkleTree.root.left=Optional.of(new MerkleElement<String>());

        MerkleElement<String> merkleElement =
            merkleTree.findOpenSlot();

        assertEquals(merkleTree.root, merkleElement);
    }

    @Test
    public void findOpenSlotTestTwoLevel(){
        var merkleTree = new MerkleTree<String>();
        merkleTree.root.left=Optional.of(new MerkleElement<String>());
        merkleTree.root.right=Optional.of(new MerkleElement<String>());

        MerkleElement<String> merkleElement = merkleTree.findOpenSlot();

        assertTrue(
            merkleElement.equals(merkleTree.root.right.get()) || 
            merkleElement.equals(merkleTree.root.left.get()) );
    }
    @Test
    public void ensureUseOfProvidedHash(){
        
        var merkleTree = new MerkleTree<testType>();
        merkleTree.add(new testType());
        merkleTree.add(new testType());
        merkleTree.add(new testType());
        merkleTree.add(new testType());
        assertEquals("1234",merkleTree.root.left.get().left.get().hash.get().getValue());
        assertEquals("1234",merkleTree.root.right.get().right.get().hash.get().getValue());
    }
    public static class testType{
        public Hash getHash(){
            return new Hash(){{
                this.value="1234";
            }};
        }
    }
}

