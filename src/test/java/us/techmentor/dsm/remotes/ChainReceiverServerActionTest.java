package us.techmentor.dsm.remotes;

import com.google.inject.Guice;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import scala.Array;
import us.techmentor.blockchain.BlockPreparation;
import us.techmentor.blockchain.Blockchain;
import us.techmentor.blockchain.Chain;
import us.techmentor.blockchain.FactOperations;
import us.techmentor.blockchain.connector.BlockchainConnector;
import us.techmentor.blockchain.connector.ServerConnector;
import us.techmentor.blockchain.connector.ServerProtocol;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.fact.FactQuery;
import us.techmentor.blockchain.persistence.ChainSerializer;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ChainReceiverServerActionTest {

    @Test
    public void serveClientTest() throws InterruptedException, IOException {
        var injector = Guice.createInjector();
        var chainSerializer = injector.getInstance(ChainSerializer.class);
        var b1 = injector.getInstance(Blockchain.class);
        var b1FactOperations = injector.getInstance(FactOperations.class);

        var mockInputStream = mock(InputStream.class);

        var finished = new AtomicBoolean(false);
        b1FactOperations.saveFact(new Fact(Map.of("id","123","message","hello world")),(b)->finished.set(true));

        while(!finished.get());
        finished.set(false);

        b1FactOperations.saveFact(new Fact(Map.of("id","456")),(b)->finished.set(true));
        while(!finished.get());

        var bytes = chainSerializer.toBytes(b1.getChain());

        when(mockInputStream.available()).thenReturn(bytes.length);
        doAnswer(invocation->{
            var arg = invocation.getArgument(0,byte[].class);
            Array.copy(bytes,0,arg,0,bytes.length);
            return bytes.length;
        }).when(mockInputStream).read(any());

        var networkClient = new NetworkClientAcceptor.NetworkClient("127.0.0.1",mockInputStream,null,null,null);
        var mockServerProtocol = mock(ServerProtocol.class);
        var mockServerOperations = mock(ServerOperations.class);
        var mockServerConnector = mock(ServerConnector.class);
        when(mockServerOperations.getServerConnectorForClient(new NetworkClientAcceptor.NetworkClient("127.0.0.1",any(),null,null,null)))
                .thenReturn(mockServerConnector);

        when(mockServerConnector.getProtocol())
                .thenReturn(mockServerProtocol);

        var chainReceiverServerAction = new ChainReceiverServerAction();
        chainReceiverServerAction.chainSerializer = chainSerializer;
        chainReceiverServerAction.serverOperations = mockServerOperations;
        chainReceiverServerAction.serveClient(networkClient);

        var chainCaptor = ArgumentCaptor.forClass(Chain.class);
        verify(mockServerProtocol).addChainToIncomingQueue(chainCaptor.capture());

        var testQuery = Map.<String, FactQuery.Truth>of("id",new FactQuery.StringTruth("123"));
        var resultFact = chainCaptor.getValue().locateFacts(new FactQuery(testQuery));

        assertEquals("hello world",resultFact.get(0).get("message"));
    }
}
