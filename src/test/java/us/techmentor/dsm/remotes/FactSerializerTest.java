package us.techmentor.dsm.remotes;

import static org.junit.Assert.assertTrue;
import java.util.Map;

import org.junit.Test;

import us.techmentor.blockchain.fact.Fact;
import us.techmentor.dsm.remotes.FactSerializer;

public class FactSerializerTest {
    @Test
    public void turnFactIntoDataTest(){
        var fact = new Fact(Map.of(
            "name","Data",
            "rank","Lieutenant Commander",
            "station", "Ops",
            "special skills","super-human strength, super-human "+
                             "processing speeds, a desire for "+
                             "emotional fulfillment"
        ));

        var factSerializer = new FactSerializer();
        var bytes = factSerializer.asBytes(fact);

        assertTrue(new String(bytes).contains("processing speeds"));
    }
    @Test
    public void turnFactIntoDataAndBackToFactTest(){
        var fact = new Fact(Map.of(
                "name","Data",
                "rank","Lieutenant Commander",
                "station", "Ops",
                "special skills","super-human strength\\, super-human "+
                        "processing speeds\\, a desire for "+
                        "emotional fulfillment"
        ));

        var factSerializer = new FactSerializer();
        var bytes = factSerializer.asBytes(fact);
        var resultFact = factSerializer.asFact(bytes);
        assertTrue(
            resultFact
            .get("special skills")
            .contains("emotional fulfillment"));
    }
}
