package us.techmentor.dsm.remotes;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import us.techmentor.blockchain.connector.BlockchainConnector;
import us.techmentor.blockchain.connector.ServerConnector;
import us.techmentor.blockchain.connector.ServerProtocol;
import us.techmentor.blockchain.fact.Fact;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static us.techmentor.dsm.remotes.NetworkClientAcceptor.*;

public class GiveFactServerActionTest {

    @Test
    public void serveClientTest() throws IOException {
        var mockOutputStream = mock(OutputStream.class);
        var mockServerOperations = mock(ServerOperations.class);
        var mockConnector = mock(ServerConnector.class);
        var mockServerProtocol = mock(ServerProtocol.class);

        when(mockServerOperations.getServerConnectorForClient(new NetworkClient("127.0.0.1",null,any(),null,null)))
            .thenReturn(mockConnector);

        when(mockConnector.getProtocol()).thenReturn(mockServerProtocol);
        when(mockServerProtocol.getNextFactFromOutgoingQueue()).thenReturn(
            Optional.of(
                Fact.of(
                "id","12345",
                "message","I ain't mad atcha'")
            )
        );

        var giveFactServerAction = new GiveFactServerAction();
        giveFactServerAction.serverOperations = mockServerOperations;
        giveFactServerAction.factSerializer = new FactSerializer();
        giveFactServerAction.serveClient(
            new NetworkClient("127.0.0.1",null,mockOutputStream,null,null)
        );

        var bytesCaptor = ArgumentCaptor.forClass(byte[].class);
        verify(mockOutputStream).write(bytesCaptor.capture());
        var factSerializer = new FactSerializer();
        var fact = factSerializer.asFact(bytesCaptor.getValue());

        assertEquals("I ain't mad atcha'",fact.get("message"));
    }
}
