package us.techmentor.dsm.remotes;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import scala.Array;
import us.techmentor.blockchain.connector.BlockchainConnector;
import us.techmentor.blockchain.connector.ServerConnector;
import us.techmentor.blockchain.connector.ServerProtocol;
import us.techmentor.blockchain.fact.Fact;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static us.techmentor.dsm.remotes.NetworkClientAcceptor.*;

public class FactReceiverServerActionTest {

    @Test
    public void serveClientTest() throws IOException {
        var factSerializer = new FactSerializer();
        var mockInputStream = mock(InputStream.class);
        var bytes =
        factSerializer.asBytes(
            new Fact(Map.of("id","123","message","hello world"))
        );
        when(mockInputStream.available()).thenReturn(bytes.length);
        doAnswer(invocation->{
            var arg = invocation.getArgument(0,byte[].class);
            Array.copy(bytes,0,arg,0,bytes.length);
            return bytes.length;
        }).when(mockInputStream).read(any());

        var networkClient = new NetworkClient("127.0.0.1",mockInputStream,null,null,null);
        var mockServerProtocol = mock(ServerProtocol.class);
        var mockServerOperations = mock(ServerOperations.class);
        var mockServerConnector = mock(ServerConnector.class);
        when(mockServerOperations.getServerConnectorForClient(new NetworkClient("127.0.0.1",any(),null,null,null)))
                .thenReturn(mockServerConnector);

        when(mockServerConnector.getProtocol())
                .thenReturn(mockServerProtocol);

        var factReceiverServerAction = new FactReceiverServerAction();
        factReceiverServerAction.factSerializer = factSerializer;
        factReceiverServerAction.serverOperations = mockServerOperations;
        factReceiverServerAction.serveClient(networkClient);

        var factCaptor = ArgumentCaptor.forClass(Fact.class);
        verify(mockServerProtocol).addFactToIncomingQueue(factCaptor.capture());
        assertEquals("hello world",factCaptor.getValue().get("message"));
    }
}
