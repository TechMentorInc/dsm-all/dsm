package us.techmentor.dsm.person;

import org.junit.Test;

import us.techmentor.Hash;

import static org.junit.Assert.assertNotNull;

public class HashTest {
    @Test
    public void hashCreationTest(){
        var person = new Person();
        person.name="Ludwig von Mises";

        var hash = Hash.create(person);
        assertNotNull(hash.getValue());
    }
}
