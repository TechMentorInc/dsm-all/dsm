package us.techmentor.dsm.person;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.junit.MockitoJUnitRunner;

import us.techmentor.Hash;
import us.techmentor.blockchain.FactOperations;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.fact.FactQuery;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

@RunWith(MockitoJUnitRunner.class)
public class ActorOperationsTest {

    @Captor
    ArgumentCaptor<Fact> factCaptor;

    @Captor
    ArgumentCaptor<ActorKey> actorKeyCaptor;

    @Captor
    ArgumentCaptor<Hash> hashCaptor;

    @Test
    public void becomeTest() throws IOException, NoSuchAlgorithmException{
        var factOperationsMock = mock(FactOperations.class);
        when(factOperationsMock.readFact(new FactQuery(any())))
            .thenReturn(Optional.of(new Fact(Map.of(
                "name","Fred",
                "hash","HashValue"
            ))));

        var actorOperations = new ActorOperations();
        actorOperations.factOperations=factOperationsMock;

        var actorFileOperationsMock = mock(ActorFileOperations.class);
        actorOperations.actorFileOperations = actorFileOperationsMock;
        
        var actorKey = ActorCrypto.getInstance().createActorKey("fredspassword");
        when(
            actorFileOperationsMock
            .getPrivateKey(
                hashCaptor.capture(),matches("fredspassword")
            )
        ).thenReturn(actorKey.getPrivateKey());

        actorOperations.become("Fred","fredspassword");

        assertEquals("HashValue",hashCaptor.getValue().getValue());
        assertEquals("Fred", actorOperations.currentActor.getName());
        assertEquals(
            actorKey.getPrivateKey(),
            actorOperations.currentActor.actorKey.getPrivateKey());
    }

    @Test
    public void createActorTest() throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, IOException{
        var actorFileOperationsMock = mock(ActorFileOperations.class);
        var factOperationsMock = mock(FactOperations.class);

        var actorOperations = new ActorOperations();
        actorOperations.factOperations=factOperationsMock;
        actorOperations.actorFileOperations=actorFileOperationsMock;
        actorOperations.createActor("Jack", "mypassword");

        verify(factOperationsMock).saveFact(factCaptor.capture(),any());
        var fact = factCaptor.getValue();
        assertEquals(fact.get("name"),"Jack");
        assertEquals(fact.get("type"),"actor-creation");

        verify(actorFileOperationsMock).savePrivateKey((Hash)any(), actorKeyCaptor.capture());

        var actorKey = actorKeyCaptor.getValue();
        var publicKeyBytes = Base64.getDecoder().decode(fact.get("public-key"));
        var keySpec = new X509EncodedKeySpec(publicKeyBytes);
        var kf = KeyFactory.getInstance("RSA");
        var publicKey = kf.generatePublic(keySpec);

        var clear = ("Four score and seven years ago our " + 
        "forefathers brought forth upon this " +
        "continent a new nation");

        var encryptionCipher = Cipher.getInstance("RSA");
        encryptionCipher.init(Cipher.ENCRYPT_MODE,actorKey.getPrivateKey());
        var cipherText = encryptionCipher.doFinal(clear.getBytes());

        var decryptionCipher = Cipher.getInstance("RSA");
        decryptionCipher.init(Cipher.DECRYPT_MODE,publicKey);
        var decryptedText = new String(decryptionCipher.doFinal(cipherText));

        assertEquals(clear,decryptedText);
    }

    // @Test
    public void createActorHashCollisionTest(){

    }

    @Test
    public void followTest() throws IOException{
        var actor1 = new Actor(){{
            this.hash=new Hash(){{
                this.value = "hash1";
            }};
        }};
        var actor2 = new Actor(){{
            this.hash=new Hash(){{
                this.value = "hash2";
            }};
        }};

        var actorFileOperationsMock = mock(ActorFileOperations.class);
        var actorOperations = new ActorOperations();
        actorOperations.actorFileOperations = actorFileOperationsMock;

        actorOperations.follow(actor1, actor2);

        verify(actorFileOperationsMock).follow(actor1.getHash(), actor2.getHash());
    }

    @Test
    public void postTest() throws Exception{
        var actorCryptoMock = mock(ActorCrypto.class);
        var factOperationsMock = mock(FactOperations.class);
        var actorOperations = new ActorOperations();
        var actor = new Actor(){{
            this.hash = new Hash(){{
                this.value="actorhash";
            }};
        }};
        actorOperations.currentActor=actor;
        actorOperations.actorCrypto=actorCryptoMock;
        actorOperations.factOperations=factOperationsMock;
        
        var testPost = "this is a test post";
        when(
            actorCryptoMock.privateKeyEncryptBase64(any(),eq(testPost))
        ).thenReturn("encrypted text");

        actorOperations.post(testPost);

        verify(factOperationsMock).saveFact(factCaptor.capture());
        var fact=factCaptor.getValue();

        assertEquals("post",fact.get("type"));
        assertEquals("encrypted text",fact.get("content"));
        assertEquals("actorhash",fact.get("actor"));
    }
    @Test
    public void getMostRecentContentByDaysTest() throws Exception{
        var factOperationsMock = mock(FactOperations.class);
        var actorCryptoMock = mock(ActorCrypto.class);
        when(actorCryptoMock.publicKeyDecryptFromBase64(any(),eq("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."))).thenReturn(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        );
        when(
        factOperationsMock
            .readFacts(any()))
            .thenReturn(
            List.of(new Fact(
                Map.of(
                    "type","post",
                    "actor","somehash",
                    "content","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                    "when",DateTime.now().toString()
                ) 
            ))
        );

        var actorOperations = new ActorOperations();
        actorOperations.factOperations = factOperationsMock;
        actorOperations.actorCrypto= actorCryptoMock;

        actorOperations.currentActor = new Actor(){{
            this.following = new ArrayList<Actor>(){{
                this.add(
                    new Actor(){{
                        this.hash = new Hash(){{
                            this.value="somehash";
                        }};
                        this.actorKey=new ActorKey();
                    }}
                );
            }};
            
        }};
        
        var content = actorOperations.getMostRecentContentByDays(5);
        assertEquals("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",content.get(0).getContentText());
    }
}
