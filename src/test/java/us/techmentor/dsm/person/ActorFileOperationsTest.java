package us.techmentor.dsm.person;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Comparator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import us.techmentor.Hash;
import us.techmentor.dsm.metadata.FileMetaDataOperations;
import us.techmentor.dsm.metadata.IdOperations;
import us.techmentor.dsm.metadata.MetaDataOperations;

public class ActorFileOperationsTest {
    @Test
    public void saveKeyTest() throws NoSuchAlgorithmException, IOException{
        var secureRandom = new SecureRandom();
        var keypairGenerator = KeyPairGenerator.getInstance("RSA");

        keypairGenerator.initialize(2048, secureRandom);
        var keypair =   keypairGenerator.generateKeyPair();
        var actorKey = new ActorKey(keypair,"mypassword");
        
        var hash = Hash.create("blah");
        var actorFileOperations = new ActorFileOperations();
        var dsmi = new MetaDataOperations(){{
            this.fileMetaDataOperations = new FileMetaDataOperations(){{
                this.idOperations = new IdOperations();
            }};
            this.forceTempHomeDirectory();
        }};
        actorFileOperations.metaDataOperations = dsmi;
        actorFileOperations.savePrivateKey(hash, actorKey);

        var f = Path.of(
            dsmi.getMetaData().dsmHomeDirectory().toString(),hash.getValue(),"privatekey").toFile();
        var in = new FileInputStream(f);
        
        var bytes = new byte[in.available()];
        in.read(bytes);
        in.close();

        var inActorKey = new ActorKey();
        inActorKey.setPrivateKey(bytes);
        inActorKey.setPassword("mypassword");

        assertArrayEquals(
            actorKey.getPrivateKey().getEncoded(),
            inActorKey.getPrivateKey().getEncoded());
    }
    @Test
    public void getPrivateKeyTest() throws NoSuchAlgorithmException, IOException{       
        var actorKey = ActorCrypto.getInstance().createActorKey("blah");

        File temp =
            Files.createTempDirectory("dsm_test_").toFile();
        var actorFileOperations = new ActorFileOperations();
        actorFileOperations.metaDataOperations =
        new MetaDataOperations(){{
            this.fileMetaDataOperations = new FileMetaDataOperations(){{
                this.idOperations = new IdOperations();
            }};
            this.manualSetHomeDirectory(temp);
        }};
        
        var hash = Hash.create("1234");
        actorFileOperations.savePrivateKey(hash, actorKey);
        
        var privateKey = actorFileOperations.getPrivateKey(hash,"blah");
        assertEquals(actorKey.getPrivateKey(), privateKey);
    }
    @Test
    public void followTest() throws IOException{
        MetaDataOperations metaDataOperations = new MetaDataOperations(){{
            this.fileMetaDataOperations = new FileMetaDataOperations(){{
                this.idOperations = new IdOperations();
            }};
        }};
        metaDataOperations.forceTempHomeDirectory();

        var actorFileOperations = new ActorFileOperations();
        actorFileOperations.metaDataOperations = metaDataOperations;
        
        var hash1 = new Hash(){{ this.value = "hash1"; }};
        var hash2 = new Hash(){{ this.value = "hash2"; }};

        actorFileOperations.follow(hash1,hash2);

        var followers = actorFileOperations.getFolloweredActors(hash1);
        followers.contains(hash2);
    }
}
